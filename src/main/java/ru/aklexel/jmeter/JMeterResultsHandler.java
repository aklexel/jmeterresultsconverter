package ru.aklexel.jmeter;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static ru.aklexel.jmeter.JMeterResultsHandler.Element.*;

/**
 * Parses a JMeter results xml and builds curls for requests.
 *
 * Created by Aleksey Koltakov on 18.07.2017.
 */
public class JMeterResultsHandler extends DefaultHandler {
    enum Element {
        HTTP_SAMPLE("httpSample"),
        REQUEST_METHOD("method"),
        REQUEST_URL("java.net.URL"),
        REQUEST_COOKIES("cookies"),
        REQUEST_HEADERS("requestHeader"),
        REQUEST_DATA("queryString"),
        RESPONSE_HEADERS("responseHeader"),
        RESPONSE_DATA("responseData"),
        OTHER("");

        private String tag;
        private StringBuilder data = new StringBuilder();

        Element(String tag) {
            this.tag = tag;
        }

        void clearData() {
            data.setLength(0);
        }

        void appendData(char[] ch, int start, int length) {
            data.append(ch, start, length);
        }

        String getData() {
            if (this == REQUEST_DATA) {
                return data.toString();
            }
            return data.toString().trim();
        }

        static Element byTag(String tag) {
            for ( Element element: Element.values() ) {
                if (element.tag.equals(tag))
                    return element;
            }

            return OTHER;
        }
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
    private final String  lineSeparator = System.lineSeparator();

    private Map<String, Integer> samples = new TreeMap<>();
    private Map<String, Writer> files = new HashMap<>();

    private Element currentElement;
    private String status;
    private String label;
    private StringBuilder curl = new StringBuilder();


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = Element.byTag(qName);
        currentElement.clearData();

        if (currentElement == HTTP_SAMPLE) {
            long ts = Long.parseLong(attributes.getValue("ts"));
            Date date = new Date(ts);
            label = String.format("%s %s (%s) %sms%s",
                    dateFormat.format(date),
                    attributes.getValue("lb"),
                    attributes.getValue("tn"),
                    attributes.getValue("t"),
                    lineSeparator);

            status = String.format("%s: %s",
                        attributes.getValue("rc"),
                        attributes.getValue("rm"));

            Integer count = samples.get(status);
            count = count == null ? 1 : count + 1;
            samples.put(status, count);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (currentElement == OTHER)
            return;

        currentElement.appendData(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (Element.byTag(qName) == HTTP_SAMPLE) {
            buildCurl();
            try {
                writeCurlToFile();
            } catch (IOException e) {
                throw new SAXException("Couldn't write result to a file:", e);
            }
        }

        currentElement = OTHER;
    }

    // builds a curl for current request and response
    private void buildCurl() {
        curl.setLength(0);
        curl.append("curl -i");
        curl.append(" -X ").append(REQUEST_METHOD.getData());
        curl.append(" '").append(REQUEST_URL.getData()).append("'");

        String[] headers = REQUEST_HEADERS.getData().split("\\n");
        for(String h: headers){
            curl.append(" -H '").append(h).append("'");
        }

        String data = REQUEST_COOKIES.getData();
        if (data.length() > 0) {
            curl.append(" -H 'Cookie: ").append(data).append("'");
        }

        data = REQUEST_DATA.getData();
        if (data.length() > 0) {
            curl.append(" --data '").append(data).append("'").append(" --compressed");
        }

        curl.append(lineSeparator).append(lineSeparator);
        data = RESPONSE_HEADERS.getData();
        if (data.length() > 0) {
            curl.append(data);
        }

        data = RESPONSE_DATA.getData();
        if (data.length() > 0) {
            curl.append(lineSeparator).append(lineSeparator).append(data);
        }
        curl.append(lineSeparator).append(lineSeparator).append(lineSeparator);
    }

    // writes request's curl and response to file
    private void writeCurlToFile() throws IOException {
        Writer writer = files.get(status);
        if (writer == null) {
            String filename = status.replaceAll("[^\\w -\\.]", "_") + ".txt";
            Path path = Paths.get(filename);
            writer = Files.newBufferedWriter(path);
            files.put(status, writer);
        }

        writer.append(label);
        writer.append(curl.toString());
    }

    @Override
    public void endDocument() throws SAXException {
        if (samples.isEmpty()) {
            System.out.println("JMeter file is empty");
            return;
        }

        samples.forEach( (status, count) -> System.out.println(status + ": " + count));
        files.values().forEach(writer -> {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    System.out.println("Couldn't close a result file:\n");
                    e.printStackTrace();
                }
            }
        });
    }

}
