package ru.aklexel.jmeter;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by Aleksey Koltakov on 18.07.2017.
 */
public class ResultsConverter {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.printf("Please specify a JMeter XML file");
            return;
        }

        File resultsFile = new File(args[0]);
        if (!resultsFile.exists()) {
            System.out.println(String.format("The file '%s' does not exist", args[0]));
            return;
        }

        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(resultsFile, new JMeterResultsHandler());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println("Couldn't parse the file:");
            e.printStackTrace();
        }
    }
}
