# README #

Simple Data Writer provides an efficient way of recording data by eliminating GUI overhead in JMeter. You can record all request and response data in XML format using this listener. Such data are very helpful for debugging your tests.

JMeterResultsConverter converts the data recorded by the Simple Data Writer listener (in XML) to list of curls with responses from the server. So, you can easily resend any request after the test just by copying and pasting a cURL into your shell.
